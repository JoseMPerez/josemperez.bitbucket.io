'use strict'

function createToolBar() {
    const BUTTON_SIZE = 22
    var tools = []
    var toolsIds = []  // canvas elements of button
    var currentButtonid // canvas Id to hold button
    var currentSelection // index of selected button

    return {

        add: (g) => {

            // tools.push(g)
            for(const n of g.nodes){
                
                tools.push(n.clone())

                if(tools.length === 1) currentButtonid = document.getElementById('toolbar');
                const button = document.createElement('CANVAS')
                document.getElementById('toolbarIconMenu').append(button)                          //May move button to draw but Not
                currentButtonid = button
                button.style.marginRight = "10px"

                toolsIds.push(button)
            }

            for(const e of g.edges)
                tools.push(e.clone())
        },

        selected: (n) => {

            currentSelection = n
        },
		
		findNode: (p) => {
			
			for(const n of tools){
				if(n.contains(p)) return n
			}
		},

        draw: () => {

            const canvas = document.getElementById('toolbar');
            // canvas.style.right = "100px"
            // canvas.style.position = "absolute"
            canvas.style.marginRight = "560px"
            const ctx = canvas.getContext('2d')

            let x = canvas.width - BUTTON_SIZE
            x = x/2 - BUTTON_SIZE/2

            // for(const c of toolsIds){
            for (let i = 0;  i < toolsIds.length; i++) {

                const c = toolsIds[i]
                const ctxB = c.getContext('2d')
                c.width = BUTTON_SIZE + 8
                c.height = canvas.height - BUTTON_SIZE/2
                
                // c.margin = 5

                // ctxB.fillStyle = 'green'
                // ctxB.fillRect(c.x,c.y, c.width, c.height)

                // if(currentButtonid === i) {

                //     const cv = toolsIds[i]
                //     const ctxButton = cv.getContext('2d')

                //     ctxButton.fillStyle =   'blue'
                //     ctxButton.fillRect(ctxButton.x,ctxButton.y, ctxButton.width, ctxButton.height)

                // } 

                const n = tools[i]

                n.draw(0, 0, BUTTON_SIZE, BUTTON_SIZE, c)



            }


// add the tools to the canvas
// you might do it here or addEventListener

            // let i = 0

            // for (const n of tools) {

            //     if(currentButtonid === i) {

            //         const cv = toolsIds[i]
            //         const ctxButton = cv.getContext('2d')

            //         ctxButton.fillStyle =   'blue'
            //         ctxButton.fillRect(ctxButton.x,ctxButton.y, ctxButton.width, ctxButton.height)

            //     } 

            //     i++
            //     n.draw(x, 10, BUTTON_SIZE, BUTTON_SIZE, canvas)
            // }
			
			// for(const n of tools){
				
			// 	n.draw(x, 10, BUTTON_SIZE, BUTTON_SIZE, canvas)
			// }
        }
		
    }
}


function createRectangleNode (color) {

    var xCoor
    var yCoor
    var width
    var height
    var canvasId

    return {
        getBounds: () => {
            return {
                x: xCoor,
                y: yCoor,
                width: width,
                height: height
            }
        },
        contains: p => {

            return (xCoor < p.x && p.x < (xCoor + width)) && (yCoor < p.y && p.y < (yCoor + height))
            
            // (xCoor + width / 2 - p.x) ** 2 + (yCoor + height / 2 - p.x) ** 2 <= width ** 2 / 4
        },
        translate: (dx, dy) => {
            xCoor += dx
            yCoor += dy
        },
        draw: (x, y, w, h, canvas) => {

            if(canvasId === undefined) {

                if (canvas !== undefined) {

                    canvasId = canvas
                    width = w
                    height = h
                    xCoor = x
                    yCoor = y
                }
                else {

                    canvasId = document.getElementById('graphpanel')
                    width = 85
                    height = 60
                    xCoor = 200
                    yCoor = 200

                }
            }
            const ctx = canvasId.getContext('2d')
            ctx.beginPath()
            ctx.strokeStyle = color
            ctx.strokeRect(xCoor, yCoor, width, height)
        },
        clone: () => {

            return createRectangleNode('black')

        }
    }
}
//======================================================================================================================

function drawGrabber(x, y) {
        
    const size = 5;
    const canvas = document.getElementById('graphpanel')
    const grab = canvas.getContext('2d')
    grab.fill()
    //grab.fillRect(x - size / 2, y - size / 2, size/2, size, size)
    grab.fillRect(x - size / 2, y - size / 2, size, size)
    grab.fillStyle= 'black'
}

function createNode (x, y, size, color) {
    return {
        getBounds: () => {
            return {
                x: x,
                y: y,
                width: size,
                height: size
            }
        },
        contains: p => {
            return (x + size / 2 - p.x) ** 2 + (y + size / 2 - p.y) ** 2 <= size ** 2 / 4
        },
        translate: (dx, dy) => {
            x += dx
            y += dy
        },
        draw: () => {
        
            const canvas = document.getElementById('graphpanel');
            const ctx = canvas.getContext('2d')
            ctx.strokeStyle = color
            ctx.strokeRect(x, y, size, size)
        }
    }
}
class Graph {
    constructor() {
        this.nodes = []
        this.edges = []
    }
    add(n) {
        this.nodes.push(n)
    }
    findNode(p) {
        for (let i = this.nodes.length - 1; i >= 0; i--) {
            const n = this.nodes[i]
            if (n.contains(p)) return n
        }
        return undefined
    }
    draw() {

        for (const n of this.nodes) {

            n.draw()
        }
    }
}


document.addEventListener('DOMContentLoaded', function () {
  const graph = new Graph()
  const n1 = createRectangleNode('goldenrod')
  const n2 = createRectangleNode('blue')
  const n3 = createRectangleNode('red')
  const toolbar = createToolBar()

  graph.add(n1)
  graph.add(n2)

  graph.add(n3)
  toolbar.add(graph)
  toolbar.draw()
  graph.draw()


  // toolbar.add(n1, "============").Icon().paintIcon(20, 20)
  //toolbar.draw()
  // toolbar.add(n1, "aquilllasnfjdb").Icon
  
  const panel = document.getElementById('graphpanel')
  let selected = undefined
  let dragStartPoint = undefined
  let dragStartBounds = undefined

  function toolbarControl(){

    const toolb = document.getElementById('toolbar')
    const ctx = canvas.getContext('2d')

    ctx.clearRect(0, 0, toolb.width, toolb.height)
    toolbar.draw()

    if (selectedButton !== undefined) {
        const bounds = selected.getBounds()
        drawGrabber(bounds.x, bounds.y)
        drawGrabber(bounds.x + bounds.width, bounds.y)
        drawGrabber(bounds.x, bounds.y + bounds.height)      
        drawGrabber(bounds.x + bounds.width, bounds.y + bounds.height)
      }  



  }

  function repaint() {
	  
	const canvas = document.getElementById('graphpanel');
	const ctx = canvas.getContext('2d')   
	  
	ctx.clearRect(0, 0, canvas.width, canvas.height)
	graph.draw()
	  
    if (selected !== undefined) {
      const bounds = selected.getBounds()
      drawGrabber(bounds.x, bounds.y)
      drawGrabber(bounds.x + bounds.width, bounds.y)
      drawGrabber(bounds.x, bounds.y + bounds.height)      
      drawGrabber(bounds.x + bounds.width, bounds.y + bounds.height)
    }    
  }
  
  function mouseLocation(event) {
    var rect = panel.getBoundingClientRect();
    return {
      x: event.clientX - rect.left,
      y: event.clientY - rect.top,
    }
  }
  
  panel.addEventListener('mousedown', event => {
    let mousePoint = mouseLocation(event)
    selected = graph.findNode(mousePoint)
    if (selected !== undefined) {
      dragStartPoint = mousePoint
      dragStartBounds = selected.getBounds()
    }
    repaint()
  })

  panel.addEventListener('mousemove', event => {
    if (dragStartPoint === undefined) return
    let mousePoint = mouseLocation(event)
    if (selected !== undefined) {
      const bounds = selected.getBounds();
      
      selected.translate(
        dragStartBounds.x - bounds.x 
          + mousePoint.x - dragStartPoint.x,
        dragStartBounds.y - bounds.y 
          + mousePoint.y - dragStartPoint.y);
      repaint()
    }
  })
  
  panel.addEventListener('mouseup', event => {
    dragStartPoint = undefined
    dragStartBounds = undefined
  })
})
