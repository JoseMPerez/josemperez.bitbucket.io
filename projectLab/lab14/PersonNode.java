import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.io.IOException;
import java.net.URL;

import javax.swing.ImageIcon;

public class PersonNode extends CircleNode
{

    private String imageURL;
    private ImageIcon icon;

    public PersonNode(Color aColor)
    {
        super(Color.WHITE);
        // TODO Auto-generated constructor stub
        super.setSize(40);
        setImageURL("http://horstmann.com/sjsu/spring2019/cs151/day14/mcconnell.png");
    }
    
    public void setImageURL(String imageURL)
    {
        this.imageURL= imageURL;
        try
        {
            icon = new ImageIcon(new URL(imageURL));
        }
        catch (IOException ex)
        {
            
        }
    }  
    
    private String getimageURL()
    {
        return imageURL;
    }
    
    
    public void draw(Graphics2D g)
    {
        Rectangle bounds = (Rectangle) this.getBounds();
        int x=(int) bounds.getX();
        int y=(int) bounds.getY();
        icon.paintIcon(null, g, x, y);
    }

}
