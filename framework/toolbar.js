'use strict'


function createToolBar() {
    const BUTTON_SIZE = 25;
    const OFFSET = 4;
    let group = []
    let tools = []
    let popup
    let popupListener

    return {
	icon: () => {
	    getIconHeight: () => BUTTON_SIZE
	    getIconWidth: () => BUTTON_SIZE
	    paintIcon: (x, y) => {
		drawGrabber(x + OFFSET, y + OFFSET)
		drawGrabber(x + OFFSET, y + BUTTON_SIZE - OFFSET)
		drawGrabber(x + BUTTON_SIZE - OFFSET, y + OFFSET)
		drawGrabber(x + BUTTON_SIZE - OFFSET, y + BUTTON_SIZE - OFFSET)
	    }
	},
	
	/**
	  * Gets the node or edge prototype that is associated with
	  * the currently selected button
	  * @return a Node or Edge prototype
	  **/
	getSelectedTool: () => {
	    for ( let i = 0; i < tools.size; i++ ) {
		let button = getComponenet(i)
		if (button.isSelected()) return tools.get(i)
	    }
	    return undefined
	},

	/**
	 * Adds a node to the tool bar.
	 * @param n the node to add
	 * @param tip the tool tip
	 **/
	addNode: (n, tip) => {
	    document.addEventListener('DOMContentLoaded', function () {
		const graph = new Graph()
		const n1 = createCircleNode(10, 10, 20, 'goldenrod')
		const n2 = createCircleNode(30, 30, 20, 'blue')
		graph.add(n1)
		graph.add(n2)
		graph.draw()
	    }
	},

	showPopup(GraphPanel, Point2D, EventListener) {

	},

	/**
	 *Adds an edge to the tool bar.
	 *@param n the node to add
	 *@param tip the tool tip
	 **/
	addEdge(e, tip) {
	    
	}
    }
}


function drawGrabber(x, y) {
    const size = 5
    const canvas = document.getElementById('graphpanel')
    const grab = canvas.getContext('2d')
    grab.fill()
    grab.fillRect(x - size / 2, y - size / 2, size/2, size, size)
    grab.fillStyle = 'black'
}
