/**
 * @author Linjun
 */
<canvas id="can" width="400" height="400"></canvas>
	<script type="text/javascript">
		var can = document.getElementById("can");
		var ctx = can.getContext("2d");
		//Create Circle Node
		function createBlock(a,b){
			ctx.beginPath();
			ctx.fillStyle = "white";
			ctx.arc(a,b,30,0,Math.PI*2);
			ctx.fill();
		}
		//Mouse
		createBlock(50,50);
		can.onmousedown = function(ev){
			var e = ev||event;
			var x = e.clientX;
			var y = e.clientY;
			drag(x,y);
		};
		//Drag function
		function drag(x,y){
			//
			if(ctx.isPointInPath(x,y)){
				//
				can.onmousemove = function(ev){
					var e = ev||event;
					var ax = e.clientX;
					var ay = e.clientY;
					//
					ctx.clearRect(0,0,can.width,can.height);
					createBlock(ax,ay);
				};
				//
				can.onmouseup = function(){
					can.onmousemove = null;
					can.onmouseup = null;
				};
			};
		}
</script>