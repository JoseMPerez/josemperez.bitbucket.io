'use strict'

/**
 * Creates a tool bar at the top of the window. Contains buttons users can use to access tools.
 **/
function createToolBar() {
    const BUTTON_SIZE = 30
    var tools = []
    var edgesIcons = []
    var toolsIds = []  // canvas elements of button
    var edgesIds = []
    var currentSelection
    var currentSelectionIdentifier

    return {

        add: (g) => {

            // Selection tool
            tools.push(true)
            const divButton = document.createElement('div')
            const button = document.createElement('CANVAS')
            divButton.appendChild(button)
            document.getElementById('toolbarIconMenu').appendChild(divButton)
            button.style.marginRight = "10px"
            toolsIds.push(button)


            // tools.push(g)
            for(const n of g.getNodePrototypes()){
                
                tools.push(n.clone())

                const divButton = document.createElement('div')
                const button = document.createElement('CANVAS')
                divButton.appendChild(button)
                document.getElementById('toolbarIconMenu').appendChild(divButton)          
                // divButton.className = 'toolbarDivs'
                button.style.marginRight = "10px"

                toolsIds.push(button)
            }

            for(const e of g.getEdgePrototypes()){

                edgesIcons.push(e)

                const divButton = document.createElement('div')
                const button = document.createElement('CANVAS')
                divButton.appendChild(button)
                document.getElementById('toolbarIconMenu').appendChild(divButton)          
                button.style.marginRight = "10px"

                edgesIds.push(button)

            }
        },

        getToolsIcons: () => {

            let canvasContainers = []

            for(const n of toolsIds) canvasContainers.push(n)

            for(const n of edgesIds) canvasContainers.push(n)

            return canvasContainers
        },

        selected: (n) => {

            if(currentSelection !== undefined) {

                currentSelection.parentElement.style.backgroundColor = "initial"
            }

            currentSelection = n
            currentSelection.parentElement.style.backgroundColor = "white"
        },

        getSelected: () => {

            if(currentSelection !== undefined) {

                for( let i = 0; i < toolsIds.length; i++){

                    if(toolsIds[i] === currentSelection){

                        currentSelectionIdentifier = false
                        return tools[i]
                    }
                }
                
                for( let i = 0; i < edgesIds.length; i++){
                    
                    if(edgesIds[i] === currentSelection) {

                        currentSelectionIdentifier = true
                        return edgesIcons[i]
                    }
                }
            }

            currentSelectionIdentifier = false
            return undefined
        },

        isEdge: () => {

            if(currentSelection !== undefined) {

                for( let i = 0; i < toolsIds.length; i++){

                    if(toolsIds[i] === currentSelection){

                        currentSelectionIdentifier = false
                    }
                }
                
                for( let i = 0; i < edgesIds.length; i++){
                    
                    if(edgesIds[i] === currentSelection) {

                        currentSelectionIdentifier = true
                    }
                }
            }
            return currentSelectionIdentifier
        },
        
        findNode: (p) => {
            
            for(const n of tools){
                if(n.contains(p)) return n
            }
        },

        draw: () => {

            const canvas = document.getElementById('toolbarIconMenu')
            canvas.height = 40

            let x = canvas.width - BUTTON_SIZE
            x = x/2 - BUTTON_SIZE/2

            for (let i = 0;  i < toolsIds.length; i++) {

                const n = tools[i]
                const c = toolsIds[i]
                const ctxB = c.getContext('2d')
                c.width = canvas.height + 5
                c.height = canvas.height

                if(i !== 0)   
                    n.draw(2, 6, canvas.height, canvas.height - 10, c)

                else {
                    
                    drawGrabber(2, 6, c)
                    drawGrabber(canvas.height, 6, c)
                    drawGrabber(canvas.height, canvas.height - 5, c)
                    drawGrabber(2, canvas.height - 5, c)
                    
                }
            }

            for(let i = 0; i < edgesIds.length; i++){

                const c = edgesIds[i]
                c.width = BUTTON_SIZE + 8
                c.height = canvas.height - BUTTON_SIZE/2
                const n = edgesIcons[i]
                n.draw(c)
            }
        }
    }
}

/**
 * Creates a rectangular node to be used
 **/
function interfaceNode (color) {
    var xCoor
    var yCoor
    var width
    var height
    var canvasId
    var text = ""
    
    return {
        getBounds: () => {
            return {
                x: xCoor,
                y: yCoor,
                width: width,
                height: height
            }
        },

        getType: () =>{

            return "NODE"
        },
        contains: p => {

            return (xCoor < p.x && p.x < (xCoor + width)) && (yCoor < p.y && p.y < (yCoor + height))
        },
        translate: (dx, dy) => {
            xCoor += dx
            yCoor += dy
        },

        setCoordinates: (x, y) => {

            xCoor = x
            yCoor = y
        },

        draw: (x, y, w, h, canvas) => {

            if(canvasId === undefined) {

                if (canvas !== undefined) {

                    canvasId = canvas
                    width = w
                    height = h
                    xCoor = x
                    yCoor = y
                }
                else {

                    canvasId = document.getElementById('graphpanel')
                    width = 85
                    height = 60
                }
            }
            const ctx = canvasId.getContext('2d')
            ctx.beginPath()
            ctx.strokeStyle = color
            ctx.strokeRect(xCoor, yCoor, width, height)
            ctx.strokeRect(xCoor + 1, yCoor + 1, width - 1, height/3)
            ctx.strokeRect(xCoor + 2, yCoor + 2, width - 2, height/3)
            ctx.strokeRect(xCoor, yCoor + height/3, width, height/3)
            ctx.font = '12px Arial'
            ctx.fillStyle = 'black'
            ctx.textAlign = 'center'
            ctx.fillText(text, xCoor + width/2, yCoor + height/2)
        },
        clone: () => {

            return interfaceNode('black')

        },
        
        setText: (newText) => {
            text = newText
        },

        getText:() => { text },

        /**
         * Get the best connection point to connect this node with another node. This
         * should be a point on the boundary of the shape of this node.
         * 
         * @param other an exterior point that is to be joined with this node
         * @return the recommended connection point
         */
        getConnectionPoint:(other) => {
            var centerX =  xCoor + (width / 2)
            var centerY =  yCoor + (height / 2)
            var dx = other.x - centerX
            var dy = other.y - centerY
            var connectX = other.x
            var connectY = other.y

            // top point
            if ( dx >= dy && dx < -dy )
            {
                connectX = xCoor + (width / 2)
                connectY = yCoor
            }
            // left point
            else if ( dx < dy && dx < -dy)
            {
                connectX = xCoor
                connectY = yCoor + (height / 2)
            }
            // bottom point
            else if (dx < dy && dx >= -dy)
            {
                connectX = xCoor + (width / 2)
                connectY = yCoor + height
            }
            // right point
            else if (dx >= dy && dx >= -dy)
            {
                connectX = xCoor + width
                connectY = yCoor + (height / 2)
            }
            
            return {
                x: connectX,
                y: connectY
            }
        }
    }
}

function createClass (color) {
    var xCoor
    var yCoor
    var width
    var height
    var canvasId
    var classes = ""
    var attributes = ""
    var methods = ""
    
    return {
        getBounds: () => {
            return {
                x: xCoor,
                y: yCoor,
                width: width,
                height: height
            }
        },

        getType: () =>{

            return "NODE"
        },

        contains: p => {

            return (xCoor < p.x && p.x < (xCoor + width)) && (yCoor < p.y && p.y < (yCoor + height))
        },
        translate: (dx, dy) => {
            xCoor += dx
            yCoor += dy
        },

        setCoordinates: (x, y) => {

            xCoor = x
            yCoor = y
        },

        draw: (x, y, w, h, canvas) => {

            if(canvasId === undefined) {

                if (canvas !== undefined) {

                    canvasId = canvas
                    width = w
                    height = h
                    xCoor = x
                    yCoor = y
                }
                else {

                    canvasId = document.getElementById('graphpanel')
                    width = 150
                    height = 150*0.618
                }
            }
            const ctx = canvasId.getContext('2d')
			const textLength = ctx.measureText(classes).width
			const textLength2 = ctx.measureText(attributes).width
			const textLength3 = ctx.measureText(methods).width
			if (textLength + 10 > width) {
				width = textLength + 30
			}
			if (textLength2 + 10 > width) {
				width = textLength2 + 30
			}
			if (textLength3 + 10 > width) {
				width = textLength3 + 30
			}
            ctx.strokeStyle = color
            ctx.strokeRect(xCoor, yCoor, width, height / 3)
            ctx.strokeRect(xCoor, yCoor + height / 3, width, height / 3)
            ctx.strokeRect(xCoor, yCoor + height * 2/ 3, width, height / 3)
            ctx.font = '12px Arial'
            ctx.fillStyle = 'black'
            ctx.textAlign = 'center'
            ctx.fillText(classes, xCoor + width/2, yCoor + height/6)
            ctx.fillText(attributes, xCoor + width/2, yCoor + height/2)
            ctx.fillText(methods, xCoor + width/2, yCoor + height*5/6)
        },
        clone: () => {

            return createClass('black')
        },
        
        setText: (newText) => {
            classes = newText
        },
        setText2: (newText) => {
            attributes = newText
        },
        setText3: (newText) => {
            methods = newText
        },
        
        getText:() => { classes },
        getText2:() => { attributes },
        getText3:() => { methods },

        /**
         * Get the best connection point to connect this node with another node. This
         * should be a point on the boundary of the shape of this node.
         * 
         * @param other an exterior point that is to be joined with this node
         * @return the recommended connection point
         */
        getConnectionPoint:(other) => {
            var centerX =  xCoor + (width / 2)
            var centerY =  yCoor + (height / 2)
            var dx = other.x - centerX
            var dy = other.y - centerY
            var connectX = other.x
            var connectY = other.y

            // top point
            if ( dx >= dy && dx < -dy )
            {
                connectX = xCoor + (width/2)
                connectY = yCoor
            }
            // left point
            else if ( dx < dy && dx < -dy)
            {
                connectX = xCoor
                connectY = yCoor + (height / 2)
            }
            // bottom point
            else if (dx < dy && dx >= -dy)
            {
                connectX = xCoor + (width / 2)
                connectY = yCoor + height
            }
            // right point
            else if (dx >= dy && dx >= -dy)
            {
                connectX = xCoor + width
                connectY = yCoor + (height / 2)
            }
            
            return {
                x: connectX,
                y: connectY
            }
        }
    }
}

function createInterface (color) {
    var xCoor
    var yCoor
    var width
    var height
    var canvasId
    var name = ""
    var methods = ""
    
    return {
        getBounds: () => {
            return {
                x: xCoor,
                y: yCoor,
                width: width,
                height: height
            }
        },
        
        getType: () =>{

            return "NODE"
        },

        contains: p => {

            return (xCoor < p.x && p.x < (xCoor + width)) && (yCoor < p.y && p.y < (yCoor + height))
        },
        translate: (dx, dy) => {
            xCoor += dx
            yCoor += dy
        },

        setCoordinates: (x, y) => {

            xCoor = x
            yCoor = y

        },

        draw: (x, y, w, h, canvas) => {

            if(canvasId === undefined) {

                if (canvas !== undefined) {

                    canvasId = canvas
                    width = w
                    height = h
                    xCoor = x
                    yCoor = y
                }
                else {

                    canvasId = document.getElementById('graphpanel')
                    width = 150
                    height = 150*0.618
                }
            }
            const ctx = canvasId.getContext('2d')
			const textLength = ctx.measureText(name).width
			const textLength2 = ctx.measureText(methods).width
			if (textLength + 10 > width) {
				width = textLength + 30
			}
			if (textLength2 + 10 > width) {
				width = textLength2 + 30
			}
            ctx.strokeStyle = color
            ctx.strokeRect(xCoor, yCoor, width, height / 2)
            ctx.strokeRect(xCoor, yCoor + height / 2, width, height / 2)
            ctx.font = '12px Arial'
            ctx.fillStyle = 'black'
            ctx.textAlign = 'center'
            ctx.fillText(name, xCoor + width/2, yCoor + height/4)
            ctx.fillText(methods, xCoor + width/2, yCoor + height*3/4)
        },
        clone: () => {

            return createInterface('black')

        },
        
        setText: (newText) => {
            name = newText
        },
        setText2: (newText) => {
            methods = newText
        },
        
        getText:() => { name },
        getText2:() => { methods },

        /**
         * Get the best connection point to connect this node with another node. This
         * should be a point on the boundary of the shape of this node.
         * 
         * @param other an exterior point that is to be joined with this node
         * @return the recommended connection point
         */
        getConnectionPoint:(other) => {
            var centerX =  xCoor + (width / 2)
            var centerY =  yCoor + (height / 2)
            var dx = other.x - centerX
            var dy = other.y - centerY
            var connectX = other.x
            var connectY = other.y

            // top point
            if ( dx >= dy && dx < -dy )
            {
                connectX = xCoor + (width/2)
                connectY = yCoor
            }
            // left point
            else if ( dx < dy && dx < -dy)
            {
                connectX = xCoor
                connectY = yCoor + (height / 2)
            }
            // bottom point
            else if (dx < dy && dx >= -dy)
            {
                connectX = xCoor + (width / 2)
                connectY = yCoor + height
            }
            // right point
            else if (dx >= dy && dx >= -dy)
            {
                connectX = xCoor + width
                connectY = yCoor + (height / 2)
            }
            
            return {
                x: connectX,
                y: connectY
            }
        }
    }
}

function createPackage (color) {
    var xCoor
    var yCoor
    var width
    var height
    var canvasId
    var text = ""
    
    return {
        getBounds: () => {
            return {
                x: xCoor,
                y: yCoor,
                width: width,
                height: height
            }
        },

        getType: () =>{

            return "NODE"
        },

        contains: p => {

            return (xCoor < p.x && p.x < (xCoor + width)) && (yCoor < p.y && p.y < (yCoor + height))
        },
        translate: (dx, dy) => {
            xCoor += dx
            yCoor += dy
        },

        setCoordinates: (x, y) => {

            xCoor = x
            yCoor = y
        },

        draw: (x, y, w, h, canvas) => {

            if(canvasId === undefined) {

                if (canvas !== undefined) {

                    canvasId = canvas
                    width = w
                    height = h
                    xCoor = x
                    yCoor = y
                }
                else {

                    canvasId = document.getElementById('graphpanel')
                    width = 150
                    height = 150*0.618

                }
            }
            const ctx = canvasId.getContext('2d')
			const textLength = ctx.measureText(text).width
			if (textLength + 10 > width) {
				width = textLength + 30
			}
            ctx.strokeStyle = color
            ctx.strokeRect(xCoor, yCoor, 0.618 * width, 0.3 * height)
            ctx.strokeRect(xCoor, yCoor + 0.3 * height, width, 0.7 * height)
            ctx.font = '12px Arial'
            ctx.fillStyle = 'black'
            ctx.textAlign = 'center'
            ctx.fillText(text, xCoor + width/2, yCoor + height*0.6854)
        },
        clone: () => {

            return createPackage('black')

        },
        
        setText: (newText) => {
            text = newText
        },
        
        getText:() => { text },

        /**
         * Get the best connection point to connect this node with another node. This
         * should be a point on the boundary of the shape of this node.
         * 
         * @param other an exterior point that is to be joined with this node
         * @return the recommended connection point
         */
        getConnectionPoint:(other) => {
            var centerX =  xCoor + (width / 2)
            var centerY =  yCoor + (height / 2)
            var dx = other.x - centerX
            var dy = other.y - centerY
            var connectX = other.x
            var connectY = other.y

            // top point
            if ( dx >= dy && dx < -dy )
            {
                connectX = xCoor + (width/2)
                connectY = yCoor
            }
            // left point
            else if ( dx < dy && dx < -dy)
            {
                connectX = xCoor
                connectY = yCoor + (height / 2)
            }
            // bottom point
            else if (dx < dy && dx >= -dy)
            {
                connectX = xCoor + (width / 2)
                connectY = yCoor + height
            }
            // right point
            else if (dx >= dy && dx >= -dy)
            {
                connectX = xCoor + width
                connectY = yCoor + (height / 2)
            }
            
            return {
                x: connectX,
                y: connectY
            }
        }
    }
}

function createNote () {
    var xCoor
    var yCoor
    var width
    var height
    var canvasId
    var text = ""
    
    return {
        getBounds: () => {
            return {
                x: xCoor,
                y: yCoor,
                width: width,
                height: height
            }
        },

        getType: () =>{

            return "NODE"
        },

        contains: p => {

            return (xCoor < p.x && p.x < (xCoor + width)) && (yCoor < p.y && p.y < (yCoor + height))
            
            // (xCoor + width / 2 - p.x) ** 2 + (yCoor + height / 2 - p.x) ** 2 <= width ** 2 / 4
        },
        translate: (dx, dy) => {
            xCoor += dx
            yCoor += dy
        },

        setCoordinates: (x, y) => {

            xCoor = x
            yCoor = y
        },

        draw: (x, y, w, h, canvas) => {

            if(canvasId === undefined) {

                if (canvas !== undefined) {

                    canvasId = canvas
                    width = w
                    height = w*0.618
                    xCoor = x
                    yCoor = y
                }
                else {

                    canvasId = document.getElementById('graphpanel')
                    width = 120
                    height = 120*0.618
                }
            }

            const ctx = canvasId.getContext('2d')
			const textLength = ctx.measureText(text).width
			if (textLength + 10 > width) {
				width = textLength + 30
			}
            ctx.fillStyle = 'lemonchiffon'
            ctx.fillRect(xCoor, yCoor, width, height)
            ctx.strokeStyle = '#000000'
            ctx.strokeRect(xCoor, yCoor, width, height)
            ctx.beginPath(); //fold
            ctx.clearRect(xCoor + width * 3 / 4, yCoor - 1, width / 4 + 1, height / 4)
            ctx.moveTo(xCoor + width * 3 / 4, yCoor)
            ctx.lineTo(xCoor + width, yCoor + height / 4)
            ctx.lineTo(xCoor + width * 3 / 4, yCoor + height / 4)
            ctx.closePath()

            ctx.moveTo(xCoor + width * 3 / 4, yCoor)
            ctx.lineTo(xCoor + width * 3 / 4, yCoor + height / 4)
            ctx.lineTo(xCoor + width, yCoor + height / 4)
            ctx.closePath()
            ctx.stroke()

            ctx.font = '12px Arial'
            ctx.fillStyle = 'black'
            ctx.textAlign = 'center'
            ctx.fillText(text, xCoor + width/2, yCoor + height/2)
            
            ctx.font = '12px Arial'
            ctx.fillStyle = 'black'
            ctx.textAlign = 'center'
            ctx.fillText(text, xCoor + width/2, yCoor + height/2)
        },
        clone: () => {

            return createNote('black')

        },
        
        setText: (newText) => {
            text = newText
        },
        
        getText:() => { text },

        /**
         * Get the best connection point to connect this node with another node. This
         * should be a point on the boundary of the shape of this node.
         * 
         * @param other an exterior point that is to be joined with this node
         * @return the recommended connection point
         */
        getConnectionPoint:(other) => {
            var centerX =  xCoor + (width / 2)
            var centerY =  yCoor + (height / 2)
            var dx = other.x - centerX
            var dy = other.y - centerY
            var connectX = other.x
            var connectY = other.y

            // top point
            if ( dx >= dy && dx < -dy )
            {
                connectX = xCoor + (width/2)
                connectY = yCoor
            }
            // left point
            else if ( dx < dy && dx < -dy)
            {
                connectX = xCoor
                connectY = yCoor + (height / 2)
            }
            // bottom point
            else if (dx < dy && dx >= -dy)
            {
                connectX = xCoor + (width / 2)
                connectY = yCoor + height
            }
            // right point
            else if (dx >= dy && dx >= -dy)
            {
                connectX = xCoor + width
                connectY = yCoor + (height / 2)
            }
            
            return {
                x: connectX,
                y: connectY
            }
        }
    }
}

/**
 * Draws the "grabber" users would see when selecting a node
 **/
function drawGrabber(x, y, canvas) {

    const size = 5
    if(canvas === undefined) canvas = document.getElementById('graphpanel')
    const grab = canvas.getContext('2d')
    //grab.fill()
    grab.fillRect(x - size / 2, y - size / 2, size, size)
    grab.fillStyle= 'black'
}

class Graph {
    constructor() {
        this.nodes = []
        this.edges = []
        this.edgesParent = []
    }
    add(n) {
        this.nodes.push(n)
    }
    findNode(p) {
        for (let i = this.nodes.length - 1; i >= 0; i--) {
            const n = this.nodes[i]
            if (n.contains(p)) return n
        }
        for (let i = this.edges.length - 1; i >= 0; i--) {
            const e = this.edges[i]
            if (e.contains(p)) return e
        }
        return undefined
    }
    draw() {

        for (const n of this.nodes) {

            n.draw()
        }
        
        for (const n of this.edges) {

            n.draw()
        }
    }
    connect(e, p1, p2) {
        const n1 = p1
        const n2 = p2
        if (n1 !== undefined && n2 !== undefined) {
            e.connect(n1, n2)
            this.edges.push(e)
            let temp = []
            temp.push(n1)
            temp.push(n2)
            this.edgesParent.push(temp)
            return true
        }
        return false
    }
    delete(node){

        if ( node.getType() == "EDGE" )
        {
            for(let i = 0; i < this.nodes.length; i++){
                if ( node === this.edges[i] ) {
                    this.edges.splice(i,1)
                }
            }
        }
        else {
            for(let i = 0; i < this.nodes.length; i++){

                let nodeRemove = this.nodes[i]

                if(node === this.nodes[i]) {

                    for(let ed = 0; ed < this.edgesParent.length; ed++){

                        let temp = this.edgesParent[ed]
                        for(let nod = 0; nod < temp.length; nod++){

                            if( nodeRemove === temp[nod]){

                                this.edges.splice(ed,1)
                                this.edgesParent.splice(ed,1)
                                ed = 0
                            }
                        }
                    }

                    this.nodes.splice(i, 1)
                }
            }
        }
    }

    clearGraph(){

        while(this.nodes.length > 0){
            this.nodes.pop()
        }
        while(this.edges.length > 0){
            this.edges.pop()
        }
    }
}

function centerOfClass(cla) {
    return { x: cla.a + cla.width / 2, y: cla.y + cla.width * 0.309 }
}

function createLineEdge() {
    let start = undefined
    let end = undefined
    let startPoint = undefined
    let endPoint = undefined
    let lineStyle = 'SOLID'
    let startArrowHead = createArrowHead()
    let endArrowHead = createArrowHead()
    let startLabel = ""
    let middleLabel = ""
    let endLabel = ""
    
    return {
        connect: (s, e) => {
            start = s
            end = e
        },

        getType: () =>{

            return 'EDGE'
        },

        setLineStyle: (ls) => {
            lineStyle = ls
        },
        
        getLineStyle: () => {
            return lineStyle
        },

        getStartArrowHead: () => {
            return startArrowHead
        },

        getEndArrowHead: () => {
            return endArrowHead
        },

        getBounds: () => {
            var xBound
            var yBound

            if ( startPoint.x < endPoint.x ) {
                xBound = startPoint.x
            }
            else if ( endPoint.x < startPoint.x ) {
                xBound = endPoint.x
            }

            if ( startPoint.y < endPoint.y ) {
                yBound = startPoint.y
            }
            else if ( endPoint.y < startPoint.y ) {
                yBound = endPoint.y
            }
            
            return {
                x: xBound,
                y: yBound,
                width: Math.abs(startPoint.x - endPoint.x),
                height: Math.abs(startPoint.y - endPoint.y)
            }
        },

        setText: (newText) => {
            startLabel = newText
        },
        setText2: (newText) => {
            middleLabel = newText
        },
        setText3: (newText) => {
            endLabel = newText
        },
        
        getText:() => { startLabel },
        getText2:() => { middleLabel },
        getText3:() => { endLabel },
        
        draw: (canvas) => {
            if(canvas === undefined) {
                const canvas = document.getElementById('graphpanel')
                const ctx = canvas.getContext('2d')
                let point1 = {
                    x: start.getBounds().x,
                    y: start.getBounds().y
                }
                let point2 = {
                    x: end.getBounds().x,
                    y: end.getBounds().y
                }
                
                startPoint = start.getConnectionPoint(point2)
                endPoint = end.getConnectionPoint(point1)
                
                ctx.beginPath()

                // Formatting for the text
                ctx.font = "12px Arial"
                ctx.textAlign = 'center'
                // start label
                ctx.fillText(startLabel, startPoint.x, startPoint.y)
                // mid label
                let midX = (startPoint.x + endPoint.x) / 2
                let midY = (startPoint.y + endPoint.y) / 2
                ctx.fillText(middleLabel, midX, midY)
                // end label
                ctx.fillText(endLabel, endPoint.x, endPoint.y)
                
                // Tells if line needs to be dashed or not
                if ( lineStyle === 'DASHED' ) {
                    //ctx.strokeStyle('red')
                    ctx.setLineDash([10,5])
                }
                
                ctx.moveTo(startPoint.x, startPoint.y)
                ctx.lineTo(endPoint.x, endPoint.y)
                ctx.stroke()

                // draws the triangles/shapes
                startArrowHead.draw(endPoint, startPoint)
                endArrowHead.draw(startPoint, endPoint)
            }
            else{
                const ctx = canvas.getContext('2d')
                ctx.beginPath()

                let xbutton = canvas.width/4
                let ybutton = canvas.height/4

                ctx.moveTo(xbutton, ybutton)
                ctx.lineTo(ybutton + canvas.width/1.5, ybutton + canvas.height/1.5)
                ctx.stroke()
            }
        },

        drawLine: (s, e) => {
            const canvas = document.getElementById('graphpanel')
            const ctx = canvas.getContext('2d')
            
            let p = s.getConnectionPoint(e)
            
            ctx.beginPath()
            ctx.moveTo(p.x, p.y)
            ctx.lineTo(e.x, e.y)
            ctx.strokeStyle = 'purple'
            ctx.stroke()
        },

        contains: (p) => {
            const MAX_DIST = 3
            
            let dist = (Math.abs((endPoint.y - startPoint.y) * p.x - 
                                 (endPoint.x - startPoint.x) * p.y + 
                                 endPoint.x * startPoint.y - 
                                 endPoint.y * startPoint.x)) /
                (Math.pow((Math.pow(endPoint.y - startPoint.y, 2) + 
                           Math.pow(endPoint.x - startPoint.x, 2)), 
                          0.5))

            return ( dist <= MAX_DIST)
        },
        
        clone: () => {
            return createLineEdge()
        },

        //dummy method, meant to do nothing and please call to translate
        translate: () => { }
    }
}

function createHVEdge() {
    let start = undefined
    let end = undefined
    let startPoint = undefined
    let endPoint = undefined
    let lineStyle = 'SOLID'
    let startArrowHead = createArrowHead()
    let endArrowHead = createArrowHead()
    let startLabel = ""
    let middleLabel = ""
    let endLabel = ""
    
    return {
        connect: (s, e) => {
            start = s
            end = e
        },

        getType: () =>{

            return "EDGE"
        },

        getLineStyle: () => {
            return lineStyle
        },

        setLineStyle: (ls) => {
            lineStyle = ls
        },
        
        draw: (canvas) => {
            if(canvas === undefined) {
                const canvas = document.getElementById('graphpanel')
                const ctx = canvas.getContext('2d')
                let point1 = {
                    x: start.getBounds().x,
                    y: start.getBounds().y
                }
                let point2 = {
                    x: end.getBounds().x,
                    y: end.getBounds().y
                }
                
                startPoint = start.getConnectionPoint(point2)
                endPoint = end.getConnectionPoint(point1)
                
                ctx.beginPath()

                // Tells if line needs to be dashed or not
                if ( lineStyle === 'DASHED' ) {
                    ctx.setLineDash([10,5])
                }
                
                //horizontal
                let midPoint = { x: endPoint.x, y: startPoint.y }
                ctx.moveTo(startPoint.x, startPoint.y)
                ctx.lineTo(midPoint.x, midPoint.y)
                //vertical
                ctx.lineTo(endPoint.x,endPoint.y)
                
                ctx.stroke()

                // draws the triangles/shapes
                startArrowHead.draw(midPoint, startPoint)
                endArrowHead.draw(midPoint, endPoint)
            }
            else{

                const ctx = canvas.getContext('2d')
                ctx.beginPath()

                let xbutton = canvas.width/4
                let ybutton = canvas.height/4

                ctx.moveTo(xbutton, ybutton)
                ctx.lineTo(ybutton + canvas.width/1.5, ybutton + canvas.height/1.5)
                ctx.stroke()
            }
        },

        drawLine: (s, e) => {
            const canvas = document.getElementById('graphpanel')
            const ctx = canvas.getContext('2d')
            
            let p = s.getConnectionPoint(e)
            
            ctx.beginPath()

            //horizontal
            ctx.moveTo(p.x, p.y)
            ctx.lineTo(e.x, p.y)
            //vertical
            ctx.lineTo(e.x,e.y)
            
            ctx.strokeStyle = 'purple'
            ctx.stroke()
        },

        contains: (p) => {
            const canvas = document.getElementById('graphpanel')
            const ctx = canvas.getContext('2d')
            
            ctx.beginPath()
            //horizontal
            ctx.moveTo(startPoint.x, startPoint.y)
            ctx.lineTo(endPoint.x, startPoint.y)
            //vertical
            ctx.lineTo(endPoint.x,endPoint.y)

            return ctx.isPointInPath(p.x, p.y)
        },
        
        clone: () => {
            return createHVEdge()
        }
    }
}

function createVHEdge() {
    let start = undefined
    let end = undefined
    let startPoint = undefined
    let endPoint = undefined
    let lineStyle = "SOLID"
    let startArrowHead = createArrowHead()
    let endArrowHead = createArrowHead()
    let startLabel = ""
    let middleLabel = ""
    let endLabel = ""
    
    return {
        connect: (s, e) => {
            start = s
            end = e
        },
        
        getType: () =>{

            return "EDGE"
        },

        getLineStyle: () => {
            return lineStyle
        },

        setLineStyle: (ls) => {
            lineStyle = ls
        },
        
        draw: (canvas) => {
            if(canvas === undefined) {
                const canvas = document.getElementById('graphpanel')
                const ctx = canvas.getContext('2d')
                let point1 = {
                    x: start.getBounds().x,
                    y: start.getBounds().y
                }
                let point2 = {
                    x: end.getBounds().x,
                    y: end.getBounds().y
                }
                
                startPoint = start.getConnectionPoint(point2)
                endPoint = end.getConnectionPoint(point1)
                
                ctx.beginPath()

                // Tells if line needs to be dashed or not
                if ( lineStyle === 'DASHED' ) {
                    ctx.setLineDash([10,5])
                }
                
                //vertical
                let midPoint = { x: startPoint.x, y: endPoint.y }
                ctx.moveTo(startPoint.x, startPoint.y)
                ctx.lineTo(midPoint.x, midPoint.y)
                //horizontal
                ctx.lineTo(endPoint.x,endPoint.y)
                
                ctx.stroke()

                // draws the triangles/shapes
                startArrowHead.draw(midPoint, startPoint)
                endArrowHead.draw(midPoint, endPoint)
            }
            else{

                const ctx = canvas.getContext('2d')
                ctx.beginPath()

                let xbutton = canvas.width/4
                let ybutton = canvas.height/4

                ctx.moveTo(xbutton, ybutton)
                ctx.lineTo(ybutton + canvas.width/1.5, ybutton + canvas.height/1.5)
                ctx.stroke()
            }
        },

        drawLine: (s, e) => {
            const canvas = document.getElementById('graphpanel')
            const ctx = canvas.getContext('2d')
            
            let p = s.getConnectionPoint(e)
            
            ctx.beginPath()

            //horizontal
            ctx.moveTo(p.x, p.y)
            ctx.lineTo(p.x, e.y)
            //vertical
            ctx.lineTo(e.x,e.y)
            
            ctx.strokeStyle = 'purple'
            ctx.stroke()
        },

        contains: (p) => {
            const canvas = document.getElementById('graphpanel')
            const ctx = canvas.getContext('2d')

            ctx.beginPath()
            //vertical
            ctx.moveTo(startPoint.x, startPoint.y)
            ctx.lineTo(startPoint.x, endPoint.y)
            //horizontal
            ctx.lineTo(endPoint.x,endPoint.y)

            return ctx.isPointInPath(p.x, p.y)
        },
        
        clone: () => {
            return createVHEdge()
        }
    }
}

/**
   Gets the path of the arrowhead
   @param p a point on the axis of the arrow head
   @param q the end point of the arrow head
   @return the path
**/
function createArrowHead() {
    var arrowType = 'NONE'

    return {
        setArrowType: (aType) => {
            arrowType = aType
        },

        getArrowType: () => {
            return arrowType
        },

        draw: (p,q) => {
            const ARROW_ANGLE = Math.PI / 6
            const ARROW_LENGTH = 10

            var dx = q.x - p.x
            var dy = q.y - p.y
            var angle = Math.atan2(dy, dx)
            var x1 = q.x - ARROW_LENGTH * Math.cos(angle + ARROW_ANGLE)
            var y1 = q.y - ARROW_LENGTH * Math.sin(angle + ARROW_ANGLE)
            var x2 = q.x - ARROW_LENGTH * Math.cos(angle - ARROW_ANGLE)
            var y2 = q.y - ARROW_LENGTH * Math.sin(angle - ARROW_ANGLE)

            // draws styles of arrowhead
            const canvas = document.getElementById('graphpanel')
            const ctx = canvas.getContext('2d')
            if ( arrowType === 'V' ) {
                ctx.moveTo(x2, y2)
                ctx.lineTo(q.x, q.y)
            }
            else if ( arrowType === 'TRIANGLE' || arrowType === 'BLACK_TRIANGLE' ) {
                ctx.lineTo(x2, y2)
                ctx.closePath()
            }
            else if ( arrowType === 'DIAMOND' || arrowType === 'BLACK_DIAMOND' ) {
                var x3 = x2 - ARROW_LENGTH * Math.cos(angle + ARROW_ANGLE)
                var y3 = y2 - ARROW_LENGTH * Math.sin(angle + ARROW_ANGLE)
                ctx.lineTo(x3, y3)
                ctx.lineTo(x2, y2)
                ctx.closePath()
            }

            // colors the arrowhead
            if ( arrowType === 'BLACK_DIAMOND' || arrowType === 'BLACK_TRIANGLE' ) {
                ctx.fillStyle = 'black'
            }
            else {
                ctx.fillStyle = 'white'
            }

            // draws arrowhead
            ctx.fill()
            ctx.strokeStyle = 'black'
            ctx.stroke()
        },
    }
}

function SimpleGraph() {

    var nodes = []
    var edges = []

    return{

        getNodePrototypes:() =>  {
            
            const n1 = createClass('goldenrod')
            const n2 = createInterface('blue')
            const n3 = createPackage('red')
            const n4 = createNote()
            
            nodes.push(n1)
            nodes.push(n2)
            nodes.push(n3)
            nodes.push(n4)
            return nodes
        },

        getEdgePrototypes:() =>  {

            const e1 = createLineEdge()
            let dash = createLineEdge()
            dash.setLineStyle('DASHED')
            const e2 = dash
            
            const hv1 = createHVEdge()
            const vh1 = createVHEdge()

            edges.push(e1)
            edges.push(e2)
            edges.push(hv1)
            edges.push(vh1)
            
            return edges
        }
    }
}


document.addEventListener('DOMContentLoaded', function () {
    const graph = new Graph()
    const e = createLineEdge()
    
    const toolbar = createToolBar()
    const simpleGraph = SimpleGraph()
    toolbar.add(simpleGraph)
    toolbar.draw()
    graph.draw()
    
    const panel = document.getElementById('graphpanel')
    const toolbarPanel = document.getElementById('toolbarIconMenu')
    let selected = undefined
    let dragStartPoint = undefined
    let dragStartBounds = undefined
    let selectionOn = undefined
    let startNode = undefined
    let edge = undefined

    let buttonsToolbar = toolbar.getToolsIcons()

    for(const n of buttonsToolbar){

        let parentDiv = n.parentElement

        parentDiv.addEventListener('click', () => toolbar.selected(n))
    }

    function repaint() {
        
        const canvas = document.getElementById('graphpanel')
        const ctx = canvas.getContext('2d')   
        
        ctx.clearRect(0, 0, canvas.width, canvas.height)
        graph.draw()
        toolbar.draw()
        
        if (selected !== undefined && selectionOn === true) {
            const bounds = selected.getBounds()
            drawGrabber(bounds.x, bounds.y)
            drawGrabber(bounds.x + bounds.width, bounds.y)
            drawGrabber(bounds.x, bounds.y + bounds.height)      
            drawGrabber(bounds.x + bounds.width, bounds.y + bounds.height)
        }    
    }

    function mouseLocation(event) {
        var rect = panel.getBoundingClientRect()
        return {
            x: event.clientX - rect.left,
            y: event.clientY - rect.top,
        }
    }
    
    panel.addEventListener('dblclick', () => {
        let mousePoint = mouseLocation(event)
        selected = graph.findNode(mousePoint)
        if (selected !== undefined) {
            //createPopUp(selected.getPropertySheet(), selected, graph);
            new Contextual({
                isSticky: false,
                items:[
                    new ContextualItem({label:selected.getText, onClick: () => {popup1()}}),
                    /*new ContextualItem({type:'submenu', label:'Item 2', submenu:[
                      new ContextualItem({label:'Subitem 1'}),
                      new ContextualItem({label:'Subitem 2'}),
                      new ContextualItem({label:'Subitem 3'}),
                      ]}),*/
                    new ContextualItem({label:selected.getText2, onClick: () => {popup2()}}),
                    new ContextualItem({label:selected.getText3, onClick: () => {popup3()}}),
                    new ContextualItem({label:'Item 5', shortcut:'Ctrl+C' }),
                ]
            });
        }
        
    }, false)
    
    function popup1() {
        var input = prompt("Please enter a new name:", selected.getText() );
        if (input == null || input == "") {
            window.alert("User cancelled the prompt.");
        } else {
            selected.setText(input);
        }
    }
    
    function popup2() {
        var input = prompt("Please enter a new attribute", selected.getText2() );
        if (input == null || input == "") {
            window.alert("User cancelled the prompt.");
        } else {
            selected.setText2(input);
        }
    }
    
    function popup3() {
        var input = prompt("Please enter a new method", selected.getText3() );
        if (input == null || input == "") {
            window.alert("User cancelled the prompt.");
        } else {
            selected.setText3(input);
        }
    }

    panel.addEventListener('mousedown', event => {

        let mousePoint = mouseLocation(event)
        selected = graph.findNode(mousePoint)
        repaint()

        if(selected !== undefined && toolbar.getSelected() !== true && toolbar.getSelected() !== undefined && toolbar.getSelected().getType() === "EDGE"){
            edge = toolbar.getSelected().clone()
            dragStartPoint = mousePoint
            startNode = selected
        }
        else if( selected === undefined  && toolbar.getSelected() !== true && toolbar.getSelected() !== undefined && toolbar.getSelected().getType() === "NODE") {
            
            edge = undefined
            selectionOn = false
            const newNode = toolbar.getSelected().clone()   
            const mousePoint = mouseLocation(event)
            newNode.setCoordinates(mousePoint.x, mousePoint.y)
            graph.add(newNode)
            repaint()
        }
        else if (selected !== undefined && toolbar.getSelected() === true) {

            edge = undefined
            selectionOn = true
            dragStartPoint = mousePoint
            dragStartBounds = selected.getBounds()
            repaint()
        }
    })

    panel.addEventListener('mousemove', event => {
        if (dragStartPoint === undefined) return
        let mousePoint = mouseLocation(event)

        if(selected !== undefined && toolbar.getSelected() !== true && toolbar.getSelected().getType() === "EDGE"){
            repaint()
            //edge = toolbar.getSelected().clone()
            edge.drawLine(startNode, mousePoint)
        }

        else if (selected !== undefined && toolbar.getSelected() === true) {
            const bounds = selected.getBounds()
            
            selected.translate(
                dragStartBounds.x - bounds.x 
                    + mousePoint.x - dragStartPoint.x,
                dragStartBounds.y - bounds.y 
                    + mousePoint.y - dragStartPoint.y)
            repaint()
        }
    })
    
    panel.addEventListener('mouseup', event => {

        let mousePoint = mouseLocation(event)
        selected = graph.findNode(mousePoint)
        
        if(selected !== undefined && selected !== startNode && toolbar.getSelected() !== undefined){
            //determines of selected tools is for Edge
            if(toolbar.getSelected() !== true && toolbar.getSelected().getType() === "EDGE"){
                graph.connect(edge, startNode, selected)
                selected = undefined
                repaint()
            }
        }

        dragStartPoint = undefined
        dragStartBounds = undefined
    })

    class Contextual{
        /**
         * Creates a new contextual menu
         * @param {object} opts options which build the menu e.g. position and items
         * @param {boolean} opts.isSticky sets how the menu apears, follow the mouse or sticky
         * @param {Array<ContextualItem>} opts.items sets the default items in the menu
         */
        constructor(opts){   
            contextualCore.CloseMenu();

            this.position = opts.isSticky != null ? opts.isSticky : false;
            this.menuControl = contextualCore.CreateEl(`<ul class='contextualJs contextualMenu'></ul>`);
            opts.items.forEach(i => {
                this.menuControl.appendChild(i.element);
            });
            
            if(event != undefined){
                event.stopPropagation()
                document.body.appendChild(this.menuControl);
                contextualCore.PositionMenu(this.position, event, this.menuControl);        
            }

            document.onclick = function(e){
                if(!e.target.classList.contains('contextualJs')){
                    contextualCore.CloseMenu();
                }
            }    
        }
        /**
         * Adds item to this contextual menu instance
         * @param {ContextualItem} item item to add to the contextual menu
         */
        add(item){
            this.menuControl.appendChild(item);
        }
        /**
         * Makes this contextual menu visible
         */
        show(){
            event.stopPropagation()
            document.body.appendChild(this.menuControl);
            contextualCore.PositionMenu(this.position, event, this.menuControl);    
        }
        /**
         * Hides this contextual menu
         */
        hide(){
            event.stopPropagation()
            contextualCore.CloseMenu();
        }
        /**
         * Toggle visibility of menu
         */
        toggle(){
            event.stopPropagation()
            if(this.menuControl.parentElement != document.body){
                document.body.appendChild(this.menuControl);
                contextualCore.PositionMenu(this.position, event, this.menuControl);        
            }else{
                contextualCore.CloseMenu();
            }
        }
    }  
    class ContextualItem{
        /**
         * 
         * @param {Object} opts
         * @param {string} [opts.label] 
         * @param {string} [opts.type]
         * @param {Array<ContextualItem>} [opts.submenu]
         * @param {string} [opts.icon]
         * @param {string} [opts.shortcut]
         * @param {void} [opts.onClick] 
         */
        constructor(opts){
            switch(opts.type){
            case 'seperator':
                this.element = contextualCore.CreateEl(`<li class='contextualJs contextualMenuSeperator'><div></div></li>`);
                break;
            case 'submenu':
            case 'normal':
            default:
                this.element = contextualCore.CreateEl( `
                    <li class='contextualJs'>
                        <div class='contextualJs contextualMenuItem'>
                            <img src='${opts.icon == undefined? '' : opts.icon}' class='contextualJs contextualMenuItemIcon'/>
                            <span class='contextualJs contextualMenuItemTitle'>${opts.label == undefined? 'No label' : opts.label}</span>
                            <span class='contextualJs contextualMenuItemOverflow ${opts.type === 'submenu'? '' : 'hidden'}'>
                                <span class='contextualJs contextualMenuItemOverflowLine'></span>
                                <span class='contextualJs contextualMenuItemOverflowLine'></span>
                                <span class='contextualJs contextualMenuItemOverflowLine'></span>
                            </span>
                            <span class='contextualJs contextualMenuItemTip'>${opts.shortcut == undefined? '' : opts.shortcut}</span>
                    </div>
                    <ul class='contextualJs contextualSubMenu contextualMenuHidden'></ul>
                    </li>`);               

                if(opts.submenu !== undefined){
                    let childMenu = this.element.querySelector('.contextualSubMenu');
                    
                    opts.submenu.forEach(i => {
                        childMenu.appendChild(i.element);
                    });
                    
                    this.element.addEventListener('click',function(e){
                        e.currentTarget.classList.toggle('SubMenuActive');
                        childMenu.classList.toggle('contextualMenuHidden');
                    });
                }else{
                    this.element.addEventListener('click', function(){
                        event.stopPropagation();
                        if(opts.onClick !== undefined){ opts.onClick(); }  
                        contextualCore.CloseMenu();
                    });
                }     
            }
        }
    }

    const contextualCore = {
        PositionMenu: (docked, el, menu) => {
            if(docked){
                menu.style.left = ((el.target.offsetLeft + menu.offsetWidth) >= window.innerWidth) ? 
                    ((el.target.offsetLeft - menu.offsetWidth) + el.target.offsetWidth)+"px"
                    : (el.target.offsetLeft)+"px";

                menu.style.top = ((el.target.offsetTop + menu.offsetHeight) >= window.innerHeight) ?
                    (el.target.offsetTop - menu.offsetHeight)+"px"    
                    : (el.target.offsetHeight + el.target.offsetTop)+"px";
            }else{
                menu.style.left = ((el.clientX + menu.offsetWidth) >= window.innerWidth) ?
                    ((el.clientX - menu.offsetWidth))+"px"
                    : (el.clientX)+"px";

                menu.style.top = ((el.clientY + menu.offsetHeight) >= window.innerHeight) ?
                    (el.clientY - menu.offsetHeight)+"px"    
                    : (el.clientY)+"px";
            }
        },
        CloseMenu: () => {
            let openMenuItem = document.querySelector('.contextualMenu:not(.contextualMenuHidden)');
            if(openMenuItem != null){ document.body.removeChild(openMenuItem); }      
        },
        CreateEl: (template) => {
            var el = document.createElement('div');
            el.innerHTML = template;
            return el.firstElementChild;
        }
    };


    const deleteNode = document.getElementById('deleteSelected')

    deleteNode.addEventListener('mousedown', event => {
        
        if(selectionOn === true && selected !== undefined){

            graph.delete(selected)
            selected = undefined
            repaint()
        }
    })

    const restartProgram = document.getElementById('restartDiagram')

    restartProgram.addEventListener('mousedown', event => {
        
        graph.clearGraph()
        repaint()
    })

    var link = document.getElementById('downloadImg')
    link.innerHTML = 'download image';
    link.addEventListener('click', function(ev) {
        link.href = panel.toDataURL();
        link.download = "mypainting.png";
    }, false);

})

